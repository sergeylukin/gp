'use strict';
const fs = require('fs')
const request = require('request')
const PornHub = require('pornhub.js')
// const xvideos = require('xvideos-lib')
const FileType = require('file-type');
const slugify = require('slugify')

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/concepts/models.html#lifecycle-hooks)
 * to customize this model
 */

module.exports = {
  lifecycles: {
    // Called before an entry is created
    async beforeCreate(data) {
      if (!data.provider) {
        console.log(data)
        throw new Error('Provider should not be blank')
      }
    },
    async beforeUpdate(params, data) {
      if (data.title && !data.slug) {
        data.slug = slugify(data.title, {lower: true}) + '-' + params.id;
      }
    },
    async afterCreate(model) {
      if (model.title && !model.slug) {
        let slug = slugify(model.title, {lower: true}) + '-' + model.id;
        await strapi.query('video').update({ id: model.id }, { slug })
      }
      if (model.provider.name === 'pornhub') {
        const pornhub = new PornHub()
          console.log('==========')
          console.log(model.remoteId)
        await pornhub.webMaster.getVideo(model.remoteId).then(async data => {
          console.log(data)
          const video = await strapi.query('video').update({ id: model.id }, {
            title: data.title,
            likes: data.vote.up,
            dislikes: data.vote.down,
          })
          console.log(model)

          const image_url = video.predefined_thumb ? video.predefined_thumb : data.thumbList[2].src
          const path = `/tmp/${model.id}`

          request(image_url).pipe(fs.createWriteStream(path)).on("finish", async () => {
            const size = fs.statSync(path).size
            const type = await FileType.fromFile(path)
            const files = {
              thumbnail: {
                path: `/tmp/${model.id}`,
                name: `thumbnail_${model.id}_2.${type.ext}`,
                type: type.mime,
                size: size,
              }
            }
            await strapi.entityService.uploadFiles(video, files, {
              model: 'video',
            });
            fs.unlinkSync(path)
          });

        })
      } else if (model.provider.name === 'xvideos') {
        // const details = await xvideos.videos.details({ url: model.url });
        // console.log(details)
          console.log('==========XVIDEOS=======')
          const video = await strapi.query('video').update({ id: model.id }, {
            title: model.title,
            likes: 0,
            dislikes: 0,
          })

          const image_url = video.predefined_thumb ? video.predefined_thumb : model.image_url
          const path = `/tmp/${model.internal_identifier}`

          request(image_url).pipe(fs.createWriteStream(path)).on("finish", async () => {
            const size = fs.statSync(path).size
            const type = await FileType.fromFile(path)
            const files = {
              thumbnail: {
                path: `/tmp/${model.internal_identifier}`,
                name: `thumbnail_${model.internal_identifier}_2.${type.ext}`,
                type: type.mime,
                size: size,
              }
            }
            await strapi.entityService.uploadFiles(video, files, {
              model: 'video',
            });
            fs.unlinkSync(path)
          });

      }
    },
  },
};
