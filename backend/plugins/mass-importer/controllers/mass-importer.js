'use strict';
const stream = require('stream');
const {promisify} = require('util');
const fs = require('fs');
const path = require('path');
const got = require('got');
const unzipper = require('unzipper');
const csv = require('csv-parser')
const readline = require('readline')

const pipeline = promisify(stream.pipeline);

/**
 * mass-importer.js controller
 *
 * @description: A set of functions called "actions" of the `mass-importer` plugin.
 */

module.exports = {

  /**
   * Default action.
   *
   * @return {Object}
   */

  index: async (ctx) => {
    // Add your own logic here.
    let params = new URLSearchParams(ctx.request.url.substring(ctx.request.url.indexOf('?'), ctx.request.url.length));
    const with_statuses = params.get('with_statuses')
    const order_by = params.get('order_by') || 'id'
    const order_direction = params.get('order_direction') || 'desc'
    const start = params.get('start') || 0
    const limit = params.get('limit') || 100
    const remote_id = params.get('remote_id') || ''
    const keywords = params.get('keywords') || ''
    const title = params.get('title') || ''
    const page = params.get('page') || 1
    const provider = params.get('provider') || 'pornhub'
    let statuses = ['pending']
    if (with_statuses) {
      statuses = with_statuses.split('|')
    }
    const filters = {
      _start: start,
      _limit: limit,
      _sort: `${order_by}:${order_direction}`,
      status_in: statuses
    }
    let result
    if (remote_id) {
      result = await strapi.query('allvideos', 'mass-importer').find({ remote_id });
    } else if (title || keywords) {
      result = await strapi.query('allvideos', 'mass-importer').model.query(qb => {
        qb
        .where('status', 'IN', statuses)
        if (title) {
          qb.whereRaw(`LOWER(title) LIKE ?`, [`%${title.toLowerCase()}%`])
        }
        if (keywords) {
          qb.where('tags', 'LIKE', `%${keywords}%`)
        }
        qb.orderBy(order_by, order_direction)
      }).fetchPage({
        pageSize: limit,
        page,
      })
    } else {
      result = await strapi.query('allvideos', 'mass-importer').find(filters);
    }

    // Send 200 `ok`
    ctx.send({
      result,
      message: 'ois'
    });
  },
  pornhubVideo: async (ctx) => {
    const { id } = ctx.params;
    const result = await strapi.query('allvideos', 'mass-importer').findOne({ id: id });

    // Send 200 `ok`
    ctx.send({
      result,
      message: 'ois',
    });
  },
  updatePornhubVideoThumbnail: async (ctx) => {
    const { id } = ctx.params;
     const {thumbnail} = ctx.request.body
     const provider = await strapi.query('provider').findOne({ name: 'pornhub' });
    const result = await strapi.query('allvideos', 'mass-importer').update({ id }, { predefined_thumb: thumbnail });

    // Send 200 `ok`
    ctx.send({
      result,
      message: 'ok'
    });
  },
  syncPornhub: async (ctx) => {
    const dumpPath = '/dumps/pornhub.zip'
    const dumpDestinationPath = '/dumps/pornhub.com-db.csv'
    const dumpExtractionPath = '/dumps'
    if (!fs.existsSync(dumpPath)) {
      await pipeline(
        got.stream('https://www.pornhub.com/files/pornhub.com-db.zip'),
        fs.createWriteStream(dumpPath)
      );
    }
    
    if (!fs.existsSync(dumpDestinationPath)) {
      await fs.createReadStream(dumpPath)
        .pipe(unzipper.Extract({ path: dumpExtractionPath }))
        .on('error', e => ctx.send({ message: e}))
        .on('finish', () => ctx.send({ message: 'done' }))
        .promise()
    }

    var rd = readline.createInterface({
      input: fs.createReadStream(dumpDestinationPath),
      output: process.stdout,
      terminal: false
    });

    for await (const row of rd) {
      const [
        iframe,
        thumbnail,
        screenshots,
        title,
        tags,
        categories,
        actors,
        duration,
        views,
        upvotes,
        downvotes
      ] = row.split('|')

      const strings = 'femdom|facesit|feet|goddess|giantess|smother|trample|whip|stockings|strapon|spanking|smoking|toilet|piss|lezdom|latex|hentai|fetish|feet|cuckold|boots|bondage|bdsm|ass|bbw';
      if (new RegExp(strings, 'i').test(title)
        || new RegExp(strings, 'i').test(categories) 
        || new RegExp(strings, 'i').test(tags)) {
          const match = iframe.match(/^.*src=\".*embed\/([a-z0-9]+)\".*<\/iframe>$/i)
          if (!match) return
            const id = match[1]
          const data = {
            remote_id: id,
            title,
            thumbnail_url: thumbnail,
            views: parseInt(views, 10) || 0,
            upvotes: parseInt(upvotes, 10) || 0,
            downvotes: parseInt(downvotes, 10) || 0,
            provider: 1
          }

          try {
            const result = await strapi.query('allvideos', 'mass-importer').create(data)
          } catch(err) { }
        }
    }

    ctx.send({
      message: 'done'
    });

    // Create temporary table "CSV" in PostgreSQL via CREATE TABLE...
    // Import all dump entries into that table using \copy command
      // COPY only  relevant rows from temp table into main "table with
      // providers entities that we use in our videos table in the end of the
      // day
  },
  xhamsterVideo: async (ctx) => {
    const { id } = ctx.params;
    const result = await strapi.query('embedxhamster', 'mass-importer').findOne({ id });

    // Send 200 `ok`
    ctx.send({
      result,
      message: 'ois'
    });
  },
  updatexhamsterVideoThumbnail: async (ctx) => {
    const { id } = ctx.params;
     const {thumbnail} = ctx.request.body
    const result = await strapi.query('embedxhamster', 'mass-importer').update({ id }, { predefined_thumb: thumbnail });

    // Send 200 `ok`
    ctx.send({
      result,
      message: 'ok'
    });
  },
   approveVideo: async (ctx) => {
     const {user} = ctx.state
     const {videoId, providerName} = ctx.request.body
     

     if (!videoId) {
       return ctx.throw(400, "Please provide a video id")
     }
     if (!providerName) {
       return ctx.throw(400, "Provider missing")
     }

     const provider = await strapi.query('provider').findOne({ name: providerName });

     const video = await strapi.query('allvideos', 'mass-importer').findOne({ id: videoId })

     let result
     if (video) {

       await strapi.query('allvideos', 'mass-importer').update({ id: videoId }, { status: 'approved' });

       console.log('REMOTE')
       console.log(video)
       console.log(video.remote_id)
       const data = { title: video.title, remoteId: video.remote_id, provider }
       if (video.thumbnail_url) {
         data.predefined_thumb = video.thumbnail_url
       }
       console.log(data)
       result = await strapi.entityService.create(
         { data },
         { model: 'video' }
       )
     } else {
       result = 'No results'
     }
     ctx.send({
       result,
     })
   },
   hideVideo: async (ctx) => {
     const {user} = ctx.state
     const {videoId,providerName} = ctx.request.body

     if (!videoId) {
       return ctx.throw(400, "Please provide a video id")
     }
     if (!providerName) {
       return ctx.throw(400, "Provider missing")
     }

     const provider = await strapi.query('provider').findOne({ name: providerName})
     const video = await strapi.query('allvideos', 'mass-importer').update({ id: videoId }, { status: 'hidden' });
     ctx.send({
       video
     })
   },
   updateSettings: async (ctx) => {
     const {user} = ctx.state
     const {pk} = ctx.request.body

     if (!pk) {
       return ctx.throw(400, "Please provide a private key")
     }

     const pluginStore = strapi.store({
       environment: strapi.config.environment,
       type: 'plugin',
       name: 'mass-importer'
     })

     const result = await pluginStore.set({ key: 'pk', value: pk })

     ctx.send({
       result,
     })
   },
   getSettings: async (ctx) => {
     const pluginStore = strapi.store({
       environment: strapi.config.environment,
       type: 'plugin',
       name: 'mass-importer'
     })

     const value = await pluginStore.get({ key: 'pk' })

     ctx.send({
       'pk': value
     })
   }
};
