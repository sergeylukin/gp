import React, {useEffect, useState} from 'react'
import {
  request
} from 'strapi-helper-plugin'

export default () => {

  const [videoCount, setVideoCount] = useState(0)

  useEffect(() => {
    const loadVideos = async () => {
      const count = await request(`${strapi.backendURL}/videos/count`)
      setVideoCount(count)
    }

    loadVideos()
  })

  return (
    <div>
      The videos count: {videoCount}
    </div>
  )
}
