/*
 *
 * HomePage
 *
 */

import React, { useState, useEffect, memo } from 'react';
// import PropTypes from 'prop-types';
import pluginId from '../../pluginId';
import { Block, Container } from '../../components/StrapiStyled'
import { Checkbox, AttributeIcon, Label, Select, Flex, InputText, Button, Padded } from '@buffetjs/core'
import {
  HeaderModal,
  HeaderModalTitle,
  Modal,
  ModalBody,
  ModalFooter,
  ModalForm,
  request
} from 'strapi-helper-plugin';
import data from '../../../../static.json'

const HomePage = () => {
  const [videos, setVideos] = useState([])
  const [selectedProvider, setSelectedProvider] = useState(0)
  const [order_by, setOrderBy] = useState('id')
  const [order_direction, setOrderDirection] = useState('desc')
  const [start, setStart] = useState(0)
  const [limit, setLimit] = useState(100)
  const [reloadVideos, setReloadVideos] = useState(false)

  const providers = [
    ...[{ id: 0, name: '', title: 'None' }],
    ...data.providers,
  ].map((item, index) => ({...item, ...{ label: item.title, value: index }}))

  const [visibleStatuses, setVisibleStatuses] = useState({
    pending: true,
    approved: false,
    hidden: false,
  });

  const [keywords, setKeywords] = useState('');
  const [title, setTitle] = useState('');
  const [remoteId, setRemoteId] = useState('');

  const handleVisibleStatusesChange = ({ target: { name, value } }) => {
    setVisibleStatuses(prevState => ({
      ...prevState,
      [name]: value,
    }));
    console.log(visibleStatuses)
    setReloadVideos(true)
  };

  const loadVideos = async () => {
    if (!selectedProvider) return
    setReloadVideos(false)
    strapi.lockApp();
    console.log(selectedProvider)
    console.log(providers[selectedProvider])
    const with_statuses = Object.keys(visibleStatuses).filter(name => visibleStatuses[name]).join('|')
    const response = await request(`/${pluginId}`, {
      method: 'GET',
      params: {
        with_statuses,
        order_by,
        order_direction,
        start,
        limit,
        keywords,
        title,
        remote_id: remoteId,
        provider: providers[selectedProvider].name
      }
    })

    const { result } = response
    setVideos(result)
    strapi.unlockApp();
  }

  const [video, setVideo] = useState({})
  const loadVideo = async (id) => {
    if (!id) return
    strapi.lockApp();
      const provider = providers[selectedProvider].name
      const response = await request(`/${pluginId}/${provider}/${id}`, {
        method: 'GET'
      })

      console.log(response)
    const { result } = response
    if (provider === 'pornhub') {
      result.thumbnails = [result.thumbnail_url]
    } else if (provider === 'xhamster') {
      const thumbnails = []
      for (let i = 1; i <= 30; i=i+3) {
        let thumb = result['image_url'].replace(/(.+\.)(\d{1,2})(\.\w{3,4})$/, `$1${i}$3`)
        thumbnails.push(thumb)
      }
      result.thumbnails = thumbnails
    }
    setVideo(result)
    strapi.unlockApp();
  }

  const updatePredefinedThumbnail = async (e) => {
    e.preventDefault()
    setReloadVideos(false)
    strapi.lockApp();
    const providerName = providers[selectedProvider].name
    const thumbnail = e.target.dataset.thumbnail

    const response = await request(`/${pluginId}/${providerName}/${video.id}/thumbnail`, {
      method: 'POST',
      body: { thumbnail }
    })
    strapi.notification.success('Selected new thumbnail')
    strapi.unlockApp();
  }

  useEffect(() => {
    loadVideos()
  }, [reloadVideos])

  const [isModalVisible, setIsModalVisible] = useState(false)
  const openPassVideoDialog = async (e) => {
    e.preventDefault()
    const videoId = e.target.dataset.videoId
    await loadVideo(videoId)
    setIsModalVisible(true)
  }
  const passVideo = async (e) => {
    e.preventDefault()
    setIsModalVisible(false)
    strapi.lockApp();
    try {
      const videoId = e.target.dataset.videoId
      await request(`/${pluginId}/approve-video`, {
        method: 'POST',
        body: { videoId, providerName: providers[selectedProvider].name }
      })
      strapi.notification.success('Posted new video')
    } catch (err) {
      strapi.notification.error(err.toString())
    }
    setReloadVideos(true)
    strapi.unlockApp();
  }

  const dismissVideo = async (e) => {
    e.preventDefault()
    strapi.lockApp();
    try {
      const videoId = e.target.dataset.videoId
      console.log(videoId)
      await request(`/${pluginId}/hide-video`, {
        method: 'POST',
        body: { videoId, providerName: providers[selectedProvider].name }
      })
      strapi.notification.success('Dismissed video ' + videoId)
    } catch (err) {
      strapi.notification.error(err.toString())
    }
    setReloadVideos(true)
    strapi.unlockApp();
  }

  return (
    <div className="row">
      {isModalVisible ? (
        <Modal isOpen={isModalVisible} onClosed={() => {}} onToggle={() => {}}>
          <HeaderModal>
            <section>
              <HeaderModalTitle>
                <span>{"Select thumbnail"}</span>
                <hr />
              </HeaderModalTitle>
            </section>
          </HeaderModal>
          <form onSubmit={() => {}}>
            <ModalForm>
              <ModalBody>
                <Container>
                  <Block>
                    <Flex justifyContent='space-between' alignItems="normal" flexWrap="wrap">
                      {video && Array.isArray(video.thumbnails) ? video.thumbnails.map(thumbnail => {
                        return <Container><a href="" onClick={updatePredefinedThumbnail} data-thumbnail={thumbnail}><img src={thumbnail} key={thumbnail} /></a></Container>
                      }) : null}
                    </Flex>
                  </Block>
                </Container>
              </ModalBody>
            </ModalForm>
            <ModalFooter>
              <section>
                <Button onClick={() => setIsModalVisible(false) } color="cancel">
                  {"Cancel"}
                </Button>
                <Button type="submit" color="success" onClick={passVideo} data-video-id={video.id}>
                  {"Go"}
                </Button>
              </section>
            </ModalFooter>
          </form>
        </Modal>
        ) : null}
        <div className="col-md-12">
          <Container>
            <Block>
              <Label htmlFor="providerSelect" message="Choose Provider" />
              <Select
                name="providerSelect"
                onChange={({ target: { value }}) => {
                  setSelectedProvider(value);
                  setReloadVideos(true)
                }}
                options={providers}
                value={selectedProvider}
              />
              {selectedProvider ? (
                <div>
                  <Flex>
                    <Padded right>
                      <Checkbox
                        name="pending"
                        message="Pending"
                        value={visibleStatuses.pending}
                        onChange={handleVisibleStatusesChange}
                      />
                    </Padded>
                    <Padded right>
                    <Checkbox
                      name="approved"
                      message="Approved"
                      value={visibleStatuses.approved}
                      onChange={handleVisibleStatusesChange}
                    />
                    </Padded>
                    <Padded right>
                      <Checkbox
                        name="hidden"
                        message="Hidden"
                        value={visibleStatuses.hidden}
                        onChange={handleVisibleStatusesChange}
                      />
                    </Padded>
                  </Flex>
                  <Flex>
                    <Block>
                      <Label htmlFor="order_by">Order by</Label>
                      <Select
                        name="order_by"
                        onChange={({ target: { value } }) => {
                          setOrderBy(value);
                        }}
                        options={['id', 'embed', 'some_id1', 'some_id2', 'some_id3', 'some_id4', 'internal_identifier']}
                        value={order_by}
                      />
                    </Block>
                    <Block>
                      <Label htmlFor="order_direction">Order direction</Label>
                      <Select
                        name="order_direction"
                        onChange={({ target: { value } }) => {
                          setOrderDirection(value);
                        }}
                        options={['asc', 'desc']}
                        value={order_direction}
                      />
                    </Block>
                  </Flex>
                  <Flex>
                    <Block>
                      <InputText
                        name="start"
                        onChange={({ target: { value } }) => {
                          setStart(value);
                        }}
                        placeholder="0"
                        type="number"
                        value={start}
                      />
                    </Block>
                    <Block>
                      <InputText
                        name="limit"
                        onChange={({ target: { value } }) => {
                          setLimit(value);
                        }}
                        placeholder="100"
                        type="number"
                        value={limit}
                      />
                    </Block>
                  </Flex>
                  <Flex>
                    <Block>
                      Keywords
                      <InputText
                        name="keywords"
                        onChange={({ target: { value } }) => {
                          setKeywords(value);
                        }}
                        placeholder=""
                        type="text"
                        value={keywords}
                      />
                    </Block>
                  </Flex>
                  <Flex>
                    <Block>
                      Title
                      <InputText
                        name="keywords"
                        onChange={({ target: { value } }) => {
                          setTitle(value);
                        }}
                        placeholder=""
                        type="text"
                        value={title}
                      />
                    </Block>
                  </Flex>
                  <Flex>
                    <Block>
                      ID
                      <InputText
                        name="remote_id"
                        onChange={({ target: { value } }) => {
                          setRemoteId(value);
                        }}
                        placeholder=""
                        type="text"
                        value={remoteId}
                      />
                    </Block>
                  </Flex>
                  <Button color="primary" onClick={loadVideos} label="Refresh" />
                  </div>
              ) : null}
              <Flex justifyContent='space-between' alignItems="normal" flexWrap="wrap">
                {videos && Array.isArray(videos) && videos.map(video => {
                  console.log(video)

                  let remote_url
                  if (providers[selectedProvider].label == 'PornHub') {
                        const videoKey = video.remote_id
                      remote_url = `https://www.pornhub.com/view_video.php?viewkey=${videoKey}`
                  } else if (providers[selectedProvider].name == 'xhamster') { 
                    remote_url = video.url
                  }
                  return (
                    <Padded key={video.id} top size="md">
                      <Block>
                        <a href={remote_url} target="_blank">
                          <img src={video.thumbnail_url} width={130} height={130} data-video-id={video.id} />
                        </a>
                        <Padded top>
                          <Flex justifyContent="space-between">
                            <Button color="secondary" label="Pass" onClick={openPassVideoDialog} data-video-id={video.id} />
                            <Button color="delete" label="Hide" onClick={dismissVideo} data-video-id={video.id} />
                          </Flex>
                        </Padded>
                      </Block>
                    </Padded>
                    )
                })}
              </Flex>
            </Block>
          </Container>
        </div>
      </div>
  );
};

export default memo(HomePage);
