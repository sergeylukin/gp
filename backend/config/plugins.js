module.exports = ({ env }) => ({
  upload: {
    provider: 'tp-minio',
    providerOptions: {
      accessKey: env('MINIO_ACCESS_KEY'),
      secretKey: env('MINIO_SECRET_KEY'),
      bucket: env('MINIO_BUCKET'),
      endPoint: 'cdn.goddess.porn',
      port: 443,
      useSSL: env('MINIO_USE_SSL') === 'true',
      folder: '',
      isDocker: false,
      host: 'cdn.goddess.porn',
    },
  },
});
// module.exports = ({ env }) => ({
//   upload: {
//     provider: 'minio',
//     providerOptions: {
//       public: env('MINIO_ACCESS_KEY'),
//       private: env('MINIO_SECRET_KEY'),
//       bucket: env('MINIO_BUCKET'),
//       endpoint: env('MINIO_ENDPOINT'),
//     },
//   },
// });
