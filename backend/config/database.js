module.exports = ({ env }) => ({
  defaultConnection: 'default',
  connections: {
    default: {
      connector: 'bookshelf',
      settings: {
        client: 'postgres',
        host: env('DATABASE_HOST', 'gp-postgres'),
        port: env.int('DATABASE_PORT', 5432),
        database: env('DATABASE_NAME', 'gp'),
        username: env('DATABASE_USERNAME', 'gp'),
        password: env('DATABASE_PASSWORD', 'gp'),
        ssl: env.bool('DATABASE_SSL', false),
      },
      options: {}
    },
  },
});
