module.exports = {
  siteMetadata: {
    title: `Goddess.Porn`,
    description: `Exotic Femdom porn video gallery`,
    author: '@gatsbyjs',
    gatsbyBackendURL: process.env.GATSBY_BACKEND_URL,
    baseURL: `https://goddess.porn/`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    "gatsby-plugin-theme-ui",
    `gatsby-theme-style-guide`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Goddess.porn`,
        short_name: `Goddess`,
        start_url: `/`,
        background_color: `#080404`,
        theme_color: `#d22626`,
        display: `minimal-ui`,
        icon: `src/images/icon.png`, // This path is relative to the root of the site.
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
    {
      resolve: `gatsby-source-strapi`,
      options: {
        apiURL: process.env.GATSBY_BACKEND_URL,
        queryLimit: 10000,
        contentTypes: [`video`, `provider`],
      },
    },
  ],
}
