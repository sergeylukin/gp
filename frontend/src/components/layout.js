/** @jsx jsx */
import { Flex, Box, Styled, jsx } from "theme-ui"
import React from "react"
import PropTypes from "prop-types"
import { useStaticQuery, graphql } from "gatsby"
import "normalize.css"
import { keyframes } from '@emotion/react'
import Header from "./header"

const fadeIn = keyframes({ from: { opacity: 0 }, to: { opacity: 1 } })

const Layout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  return (
    <Styled.root
      sx={{
        animationName: fadeIn,
        animationDuration: '2s',
        animationFillMode: 'ease-in-out',
      }}>
      <Header siteTitle={data.site.siteMetadata.title} />
      <main>
        <Box sx={{ variant: "variants.siteContentArea" }}>{children}</Box>
      </main>
      <footer>
        <Styled.p sx={{ textAlign: 'center' }}>
          © {new Date().getFullYear()}
        </Styled.p>
      </footer>
    </Styled.root>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
