/** @jsx jsx */
import { Flex, Box, Styled, jsx } from "theme-ui"
import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import Logo from "../images/logo.png"

const Header = ({ siteTitle }) => (
  <Styled.div
    as="header"
    sx={{ variant: 'variants.siteHeader' }}
  >
    <Flex
      sx={{
        px: [null, null, null, 3],
        justifyContent: 'center',
      }}>
        <Link
          to="/"
          sx={{
            color: `muted`,
            textDecoration: `none`,
          }}
        >
          <img src={Logo} alt="https://Goddess.porn/" sx={{ width: '100%', maxWidth: 777 }} />
        </Link>
    </Flex>
  </Styled.div>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
