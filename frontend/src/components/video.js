import React from 'react'
import Layout from './layout'
import { Embed, Text, Flex, Styled, jsx, Grid, Box } from "theme-ui"

export default ({ pageContext: {id, title, remoteId, providerName} }) => {
  let src = `https://www.pornhub.com/embed/${remoteId}`
  if (providerName === 'xvideos') {
    src = `https://www.xvideos.com/embedframe/${remoteId}`
  }
  return (
    <Layout>
      <Flex
        sx={{
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
        }}
        >
        <Box sx={{
          position: "relative",
          width: "100%",
          maxWidth: "1240px",
          height: 0,
          paddingTop: "2rem",
          paddingBottom: "calc(var(--aspect-ratio, .5625) * 100%)",
        }}>
          <Embed
            src={src}
            sx={{
              position: "absolute",
              top: 0,
              left: 0,
              width: "100%",
              height: '100%',
            }}
            frameBorder={0}
            scrolling="no"
            allowFullScreen
            title={title}
          />
        </Box>
        <Text
          sx={{
            pt: 3,
            fontSize: 4,
            fontWeight: 'bold',
            textAlign: 'center',
          }}>{title}</Text>
      </Flex>
    </Layout>
  )
}
