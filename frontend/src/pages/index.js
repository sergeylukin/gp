/** @jsx jsx */
import React from "react"
import { Link, graphql } from "gatsby"
import { Flex, Styled, jsx, Grid, Box } from "theme-ui"
import Img from 'gatsby-image'

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

const IndexPage = ({ data }) => {
  console.log(data)
  return (
    <Layout>
      <SEO title="Home" />
      <Box sx={{ variant: 'variants.siteContainer' }}>
        <Grid
          gap={[2, 1, null, null]}
          columns={[ 2, 3, 4, 5 ]}>
          {data.allStrapiVideo.nodes.map(({ id, title, thumbnail, remoteId, slug }) => {
            let coverImage = <div>{"Placeholder"}</div>
            if (thumbnail) {
              coverImage = <Img fixed={thumbnail.childImageSharp.fluid} sx={{ minWidth: '100px', minHeight: '100px', width: '100%', border: '2px solid #f8e0c1', borderRadius: '4%' }} />
            }
            return (
              <Box key={id} p={[1, 2, 3, null]}
                sx={{
                  display: 'flex',
                  justifyContent: 'center',
                  flexDirection: 'column',
                  // background: 'white',
                  // m: [2, 3, null, null],
                }}
                >
                <Link to={`/vid/${slug}`} sx={{ alignSelf: 'center', display: 'block' ,width: '100%' }}>{coverImage}</Link>
                  <p
                    sx={{
                      textAlign: 'center',
                      height: '1rem',
                      overflow: 'hidden',
                      whiteSpace: 'nowrap',
                      textOverflow: 'ellipsis'
                    }}
                    >{title}</p>
                {/*
                  <iframe src={iframe_src} frameborder={0} width="560" height="315" scrolling="no" allowfullscreen title={title} />
                  */}
              </Box>
            )
          })}
        </Grid>
      </Box>
    </Layout>
  )
}

export default IndexPage

export const pageQuery = graphql`
  query {
    allStrapiVideo(sort: {fields: id, order: DESC}) {
      nodes {
        id
        remoteId
        title
        slug
        thumbnail {
          childImageSharp {
            fluid(maxWidth: 200) {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
  }
`
