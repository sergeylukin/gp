/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */
const path = require('path')

exports.createPages = async ({ actions: { createPage }, graphql}) => {
  await graphql(`
    {
      allStrapiVideo {
        nodes {
          remoteId
          title
          id
          slug
          provider {
            id
            name
          }
        }
      }
    }
  `).then(res => {
    console.log(res)
    res.data.allStrapiVideo.nodes.forEach(({id, title, remoteId, slug, provider}) => {
      createPage({
        path: `/vid/${slug}`,
        component: require.resolve('./src/components/video'),
        context: {
          id,
          title,
          remoteId,
          providerName: provider.name
        }
      })
    })
  })
}

